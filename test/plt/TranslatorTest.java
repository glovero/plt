package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate(inputPhrase));
	}

	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownkay", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseWithCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseWithMoreWords() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseContainingWordEndingWithPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseContainingWordStartingWithPunctuations() {
		String inputPhrase = "well-being (composite word)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay (ompositecay ordway)", translator.translate(inputPhrase));
	}
	
	@Test
	public void testTranslationPhraseContainingWordStartingAndEndingWithPunctuations() {
		String inputPhrase = "hello (world)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay (orldway)", translator.translate(inputPhrase));
	}
}
