package plt;

public class Translator {
	
	public static final String NIL = "nil";
	public static final String[] CHARACTERS = {".", ",", ";", ":", "?", "!", "'", "(", ")"};
	
	private String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate(String phrase) {
		int i = 0;
		boolean wordStartsWithPunctuation = false;
		boolean wordEndsWithPunctuation = false;
		
		if(phrase == "") {
			return NIL;
		}
		
		if(phrase.contains(" ")) {
			int spaces = 0;
			char x;
			for (int y = 0; y < phrase.length(); y++) {
				x = phrase.charAt(y);
				if (x == ' ') {
					spaces++;
				}
			}
			String[] phraseMoreWords = phrase.split(" ");
			String translatedPhrase;
			for (int index = 0; index < 9; index++) {
			if(phraseMoreWords[0].endsWith(CHARACTERS[index])) {
				wordEndsWithPunctuation = true;
				}	
			}
			if(wordEndsWithPunctuation) {
				translatedPhrase = translate(phraseMoreWords[0].substring(0, (phraseMoreWords[0].length() - 1))) + phraseMoreWords[0].charAt(phraseMoreWords[0].length() - 1) + " ";
			} else translatedPhrase = translate(phraseMoreWords[0]) + " ";
			for (int z = 0; z < spaces; z++) {
				wordStartsWithPunctuation = false;
				wordEndsWithPunctuation = false;
				for (int index1 = 0; index1 < 9; index1++) {
					if(phraseMoreWords[z + 1].startsWith(CHARACTERS[index1])) {
						wordStartsWithPunctuation = true;
					}	
				}
				
				for (int index2 = 0; index2 < 9; index2++) {
					if(phraseMoreWords[z + 1].endsWith(CHARACTERS[index2])) {
						wordEndsWithPunctuation = true;
					}	
				}
				
				if(wordStartsWithPunctuation && !wordEndsWithPunctuation) {
					translatedPhrase += phraseMoreWords[z + 1].charAt(0) + translate(phraseMoreWords[z + 1].substring(1, (phraseMoreWords[z + 1].length()))) + " ";
				}
				
				if(wordEndsWithPunctuation && !wordStartsWithPunctuation) {
					translatedPhrase += translate(phraseMoreWords[z + 1].substring(0, (phraseMoreWords[z + 1].length() - 1))) + phraseMoreWords[z + 1].charAt(phraseMoreWords[z + 1].length() - 1) + " ";
				}
				
				if(!wordStartsWithPunctuation && !wordEndsWithPunctuation) {
					translatedPhrase += translate(phraseMoreWords[z + 1]) + " ";
				}
				
				if(wordStartsWithPunctuation && wordEndsWithPunctuation) {
					translatedPhrase += phraseMoreWords[z + 1].charAt(0) + translate(phraseMoreWords[z + 1].substring(1, (phraseMoreWords[z + 1].length() - 1))) + phraseMoreWords[z + 1].charAt(phraseMoreWords[z + 1].length() - 1) + " ";
				}
			}
			return translatedPhrase.substring(0, (translatedPhrase.length() - 1));
		}
		
		if(phrase.contains("-")) {
			String[] compositeWord = phrase.split("-");
			return translate(compositeWord[0]) + "-" + translate(compositeWord[1]);
		}
			
		if(startWithVowel(phrase)) {
			if(phrase.endsWith("y")) {
				return phrase + "nay";
			} else if(endWithVowel()) {
				return phrase + "yay";
			} else if(!endWithVowel()) {
				return phrase + "ay";
			}
		}
		
		if(startWithVowel(phrase.substring(1))) {
			return phrase.substring(1) + phrase.charAt(0) + "ay" ;
		} else while(!startWithVowel(phrase.substring(i))) { 
			i++;
		}
		return phrase.substring(i) + phrase.substring(0, i - 1) + "ay" ;
	}
	
	private boolean startWithVowel(String inputPhrase) {
		return inputPhrase.startsWith("a") || inputPhrase.startsWith("e") || inputPhrase.startsWith("i") || inputPhrase.startsWith("o") || inputPhrase.startsWith("u");
	}
	
	private boolean endWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
	}

}
